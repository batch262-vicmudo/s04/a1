-- Activity s04
-- a. Find all artists that have letter d in its name.
SELECT name FROM artists WHERE artists.name LIKE "%d%";
-- b. Find all songs that have a length of less than 230.
SELECT * FROM songs WHERE songs.length < '00:03:30';
-- c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length)
SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;
-- d. Join the 'artists' and 'albums' tables.
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";
-- e. Sort the albums in Z-A order.
SELECT * FROM albums ORDER BY albums.album_title DESC LIMIT 4;
-- f. Join the 'albums' and 'songs' table. 
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY albums.album_title DESC;